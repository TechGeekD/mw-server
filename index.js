var sql = require( "seriate" );  
var http = require("http");
var express = require('express')
var schema = require( "./schema" );
var fs = require('fs');

var app = express()

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


// config settings for SQL database
    // var config = {  
    //     "server": "localhost\\sql",
    //     "user": "sa",
    //     "password": "48122112",
    //     "database": "metta"
    // };

    var config = {  
        "server": "den1.mssql5.gear.host",
        "user": "mettaw",
        "password": "",
        "database": "mettaw"
    };

//Dynamic PORT Number or SET DEFAULT 90
var port = process.env.PORT || 90;

sql.setDefaultConfig( config );

http.createServer(function (req, resp) {

    console.log("Processing...");
    switch (req.method) {
        case "GET":
            console.log("GET");
            if(req.url === "/"){
                resp.writeHead(200, {"Content-Type": "text/html"});
                resp.write("<!DOCTYPE \"html\">");
                resp.write("<html>");
                resp.write("<head>");
                resp.write("<title>H-Ac-k-Er</title>");
                resp.write("</head>");
                resp.write("<body  bgcolor='black' style='color: limegreen'>");
                resp.write("<br><br><br><br><br><br><br><br><br><br><br><br>");
                resp.write("<center><h1>Seriate Server <small><sub><sub>By TechGeekD</sub></small></h1>");
                resp.write("</body>");
                resp.write("</html>");
                resp.end();
            } else if (req.url === "/users") {
                console.log("get User Details");
                var allUserData = "";
                schema.getAllUsersInfo( )
                    .then( function( results ) {
                        results.forEach(function(element) {
                            allUserData += JSON.stringify(element[0]);
                        }, this);
                        var indexLast = allUserData.lastIndexOf("]")+1; 
                        allUserData = allUserData.substring(0,indexLast); //to remove appending "undefined" after json 
                        var data = JSON.parse(allUserData);
                        data.forEach(function(element) {
                            console.log("User_id: "+element.user_id+" & User_Name: "+element.uname);        
                        }, this);
                        if( allUserData[0] != undefined ){
                            resp.write(allUserData);
                        } else {
                            resp.write(JSON.stringify("err"));
                            console.log("No UserData Found!")
                        }
                        resp.end();   
                    }, function( err ) {
                            console.log( "Something bad happened:", err );
                        } );
            } else if (req.url === "/admin/users") {
                console.log("get AdminUser Details");
                schema.getAllUsersInfo( )
                    .then( function( results ) {
                        if( results[0][0] != undefined ){
                            resp.write(JSON.stringify(results[0][0]));
                        } else {
                            resp.write(JSON.stringify("err"));
                            console.log("No UserData Found!")
                        }
                        resp.end();   
                    }, function( err ) {
                            console.log( "Something bad happened:", err );
                        } );
            } else if (req.url == "/users/fpAuthToken/get") {
                console.log("Get All Finger Tokens");                
                schema.getAllFpAuthToken( )
                        .then( function( results ){
                            console.log( results[0][0] );
                            resp.write(JSON.stringify(results[0][0]));
                            resp.end();
                        }, function( err ) {
                            console.log( "Something bad happened:", err );
                        });
            } else if (req.url === "/events"){
                console.log("get Event Details");
                var allEventsData = "";
                schema.getAllEventsInfo( )
                    .then( function( results ) {
                        results.forEach(function(element) {
                            allEventsData += JSON.stringify(element[0]);
                        }, this);
                        var indexLast = allEventsData.lastIndexOf("]")+1;
                        allEventsData = allEventsData.substring(0,indexLast); //to remove appending "undefined" after json
                        var data = JSON.parse(allEventsData);
                        data.forEach(function(element) {
                            console.log("Event_id: "+element.ev_id+" & Event_Name: "+element.event_name);        
                        }, this);
                        if( data[0] != undefined ){
                            resp.write(allEventsData);
                        } else {
                            resp.write(JSON.stringify("err"));
                        }
                        resp.end();
                    });

            } else if (req.url === "/admin/events"){
                console.log("get AdminEvent Details");
                var allEventsData = "";
                schema.getAdminAllEventsInfo( )
                    .then( function( results ) {
                        results.forEach(function(element) {
                            allEventsData += JSON.stringify(element[0]);
                        }, this);
                        var indexLast = allEventsData.lastIndexOf("]")+1;
                        allEventsData = allEventsData.substring(0,indexLast); //to remove appending "undefined" after json
                        var data = JSON.parse(allEventsData);
                        data.forEach(function(element) {
                            console.log("Event_id: "+element.ev_id+" & Event_Name: "+element.event_name);        
                        }, this);
                        if( data[0] != undefined ){
                            resp.write(allEventsData);
                        } else {
                            resp.write(JSON.stringify("err"));
                        }
                        resp.end();
                    });

            } else if (req.url === "/admin/events/requests"){
                console.log("get AdminEvent Requests");
                schema.getAdminAllEventsRequest( )
                    .then( function( results ) {
                        console.log(results[0][0]);
                        if( results[0][0] != undefined ){
                            resp.write(JSON.stringify(results[0][0]));
                        } else {
                            resp.write(JSON.stringify("err"));
                        }
                        resp.end();
                    });

            } else if (req.url === "/admin/events/joinRequests"){
                console.log("get AdminEvent Requests");
                schema.getAdminAllEventJoinReq( )
                    .then( function( results ) {
                        console.log(results[0][0]);
                        if( results[0][0] != undefined ){
                            resp.write(JSON.stringify(results[0][0]));
                        } else {
                            resp.write(JSON.stringify("err"));
                        }
                        resp.end();
                    });

            } else if (req.url == "/request/get") {
                console.log("Get Request For Event");                
                schema.getEventRequest( )
                        .then( function( results ){
                            console.log( results[0][0] );
                            resp.write(JSON.stringify(results[0][0]));
                            resp.end();
                        }, function( err ) {
                            console.log( "Something bad happened:", err );
                        });
            } else if (req.url == "/events/interest/get") {
                console.log("Get Interest");                
                schema.getAllInterest( )
                        .then( function( results ){
                            console.log( results[0][0] );
                            resp.write(JSON.stringify(results[0][0]));
                            resp.end();
                        }, function( err ) {
                            console.log( "Something bad happened:", err );
                        });
            } else if (req.url == "/admin/stats/get") {
                console.log("Get Interest");                
                schema.getAdminStats( )
                        .then( function( results ){
                            console.log( results[0] );
                            resp.write(JSON.stringify(results[0]));
                            resp.end();
                        }, function( err ) {
                            console.log( "Something bad happened:", err );
                        });
            } else {
                // var patt = "[^/users]+[a-zA-Z]+";
                // var urlPatt = new RegExp("/users/" + patt, "g");
                var uname =  req.url.substring(7,req.url.length); 
                // console.log(urlPatt);
                console.log(req.url);
                console.log("get users");
                // if (urlPatt.test(req.url)) {
                //         patt = new RegExp(urlPatt);
                //         console.log(patt);
                //         uname = patt.exec(req.url);
                        console.log("user_name: "+uname);
                        schema.getUserProfile( uname )
                            .then( function( results ) {
                                console.log( results[0][0] );
                                resp.write(JSON.stringify(results[0][0]));
                                resp.end();   
                            }, function( err ) {
                                console.log( "Something bad happened:", err );
                            });
                // }         
            }   
            break;
        case "POST":
            console.log("POST");
            if(req.url === "/users"){
                var reqBody = "";
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into PUSH CASE/req.on");
                });
                req.on("end", function () {
                    reqBody = JSON.parse(reqBody);
                    console.log(reqBody);
                schema.insertUsersInfo( reqBody.uname, reqBody.fullname, reqBody.passwordx, reqBody.email, reqBody.phone, reqBody.statusx )  
                    .then( function( results ) {
                        console.log( results);
                        console.log( results[0]);
                        resp.write(JSON.stringify(results[0]));
                        resp.end();
                    }, function( err ) {
                            console.log( "Something bad happened:", err );
                        } );
                });
                
            } else if (req.url === "/users/login"){
                console.log("login users");
                
                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse(reqBody);
                    schema.loginUsers( reqBody.uname, reqBody.passwordx )  
                        .then( function( results ) {

                            if(results[0][0][0]){
                            console.log("username: "+results[0][0][0].uname+" & password: "+results[0][0][0].passwordx);
                            resp.write(JSON.stringify(results[0][0][0]));
                            } else {
                            resp.write('{ "error": "Login Error !!"}');
                            console.log("Login Error !!");
                            }
                            resp.end();
                        }, function( err ) {
                                console.log( "Something bad happened:", err );
                            } );

                });
            } else if (req.url == "/users/fpAuthToken/set") {
                console.log("Set User Finger Token");
                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    schema.setFpAuthToken( reqBody.user_id, reqBody.token )
                            .then( function( results ){
                                console.log(results);
                                resp.write('200');
                                resp.end();
                            }, function( err ) {
                                console.log( "Something bad happened:", err );
                            });
                });  
            } else if (req.url === "/events"){
                console.log("get Event List");
                var allEventsData = "";
                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse(reqBody);
                    console.log(reqBody);
                    schema.getAllEventsInfo( reqBody.category )
                        .then( function( results ) {
                            results.forEach(function(element) {
                                allEventsData += JSON.stringify(element[0]);
                            }, this);
                            var indexLast = allEventsData.lastIndexOf("]")+1;
                            allEventsData = allEventsData.substring(0,indexLast); //to remove appending "undefined" after json
                            var data = JSON.parse(allEventsData);
                            data.forEach(function(element) {
                                console.log("Event_id: "+element.ev_id+" & Event_Name: "+element.event_name);        
                            }, this);
                            if( data[0] != undefined ){
                                resp.write(allEventsData);
                            } else {
                                resp.write(JSON.stringify("err"));
                            }
                            resp.end();
                        });
                });
            } else if (req.url === "/events/add") {
                console.log("insert New Event");

                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse(reqBody);
                    schema.insertEventInfo( reqBody.event_name, reqBody.event_desc, reqBody.event_city, reqBody.category )  
                        .then( function( results ) {
                            console.log( results);
                            console.log( results[0]);
                            resp.write(JSON.stringify(results[0]));
                            resp.end();
                        }, function( err ) {
                            console.log( "Something bad happened:", err );
                            } );

                });
                
            } else if (req.url === "/events/join") {
                console.log("join New Event");

                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse(reqBody);
                    schema.joinEvents( reqBody.user_id, reqBody.group_id, reqBody.ev_id, reqBody.type )  
                        .then( function( results ) {
                            console.log( results[0]);
                            resp.write(JSON.stringify(results[0]));
                            resp.end();
                        }, function( err ) {
                            console.log( "Something bad happened:", err );
                            } );

                });
                
            } else if (req.url === "/events/group/join") {
                console.log("groupJoin New Event");

                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    schema.joinEvents( reqBody.user_id, reqBody.group_id, reqBody.ev_id, reqBody.type )  
                        .then( function( results ) {
                            console.log( results[0]);
                            resp.write(JSON.stringify("200"));
                            resp.end();
                        }, function( err ) {
                            console.log( "Something bad happened:", err );
                            } );

                });
                
            } else if (req.url === "/events/joined") {
                console.log("get joined Event");

                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse(reqBody);
                    console.log(reqBody);
                    schema.getAllUsersEvents( reqBody.uname )  
                        .then( function( results ) {
                            console.log( results[0][0]);
                            resp.write(JSON.stringify(results[0][0]));
                            resp.end();
                        }, function( err ) {
                            console.log( "Something bad happened:", err );
                            } );

                });
                
            } else if (req.url === "/events/details") {
                console.log("get Event Details");

                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse(reqBody);
                    console.log( reqBody );
                    schema.getEventsDetails( reqBody.ev_id )  
                        .then( function( results ) {
                            console.log( results[0] );
                            resp.write(JSON.stringify(results[0]));
                            resp.end();
                        }, function( err ) {
                            console.log( "Something bad happened:", err );
                            } );

                });
                
            } else if (req.url === "/events/participant") {
                console.log("get Event Participant");

                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse(reqBody);
                    console.log( reqBody );
                    schema.getEventsParticipant( reqBody.ev_id )  
                        .then( function( results ) {
                            console.log( results[0][0] );
                            resp.write(JSON.stringify(results[0][0]));
                            resp.end();
                        }, function( err ) {
                            console.log( "Something bad happened:", err );
                            } );

                });
                
            } else if (req.url === "/media/image") {
                console.log("Upload image");

                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody.username, reqBody.event_name);
                    schema.manageImage( reqBody.file, reqBody.username, reqBody.event_name );
                });
                resp.end();
                
            } else if (req.url === "/media/image/get") {
                console.log("Fetch image");

                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    schema.getPic( reqBody.username, reqBody.event_name )
                        .then( function( results ) {
                            console.log(results[0][0]);
                                // file = results[0][0][0].pic;
                                // read binary data
                                // var bitmap = fs.readFileSync( file );
                                // convert binary data to base64 encoded string
                                // var img = "data:image/jpeg;base64,";
                                // img += new Buffer(bitmap).toString('base64');
                                resp.write(JSON.stringify(results[0][0]));
                                // img = "nodata";
                                resp.end();
                            }, function( err ) {
                                console.log( "Something bad happened:", err );
                                } );
                });
                
            } else if (req.url === "/media/insert") {
                console.log("Upload Media");

                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    // console.log(reqBody);
                    schema.insertMedia( reqBody.file, reqBody.med_type, reqBody.category, reqBody.ev_id, reqBody.event_name );
                });
                resp.end();
                
            } else if (req.url === "/media/get") {
                console.log("Fetch Media");

                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    schema.getMedia( reqBody.ev_id, reqBody.type )
                        .then( function( results ) {
                            console.log(results[0][0]);
                                resp.write(JSON.stringify(results[0][0]));
                                resp.end();
                            }, function( err ) {
                                console.log( "Something bad happened:", err );
                                } );
                });
                
            } else if (req.url === "/users/find") {
                console.log("Find User");

                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    schema.findUser( reqBody.uname )
                        .then( function( results ) {

                            if( results[0][0][0] == undefined){
                                resp.write(JSON.stringify( { err: 'notfound' } ));
                                console.log("User Not Found !");
                            } else {
                                console.log( results[0][0] );
                                resp.write(JSON.stringify( results[0][0][0] ));
                            }
                            resp.end();
                        }, function( err ) {
                            console.log( "Something bad happened:", err );
                            } );
                });
                
            } else if (req.url == "/notify") {
                console.log("get Notification");
                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    // schema.getMaxNotifyId ( )
                    //         .then( function( results ){
                    //             console.log( "Get Max Notification ID" );
                    //             console.log( results[0] );
                    //             schema.setNotifyId ( reqBody.uname, results[0].nid )
                    //                     .then( function( results ){
                    //                         console.log("Set Notification Id")
                    //                         schema.getNotify( reqBody.uname )
                    //                                 .then( function( results ) {
                    //                                     console.log( results );
                    //                                     resp.write(JSON.stringify(results));
                    //                                     resp.end();   
                    //                                 }, function( err ) {
                    //                                     console.log( "Something bad happened:", err );
                    //                                 });
                    //                     }, function( err ){
                    //                         console.log( "Something bad happened:", err );
                    //                     }); 
                    //         }, function( err ) {
                    //             console.log( "Something bad happened:", err );
                    //         });
                    schema.getNotify( reqBody.user_id, reqBody.maxNotifyId )
                            .then( function( results ) {
                                console.log( results[0][0] );
                                resp.write(JSON.stringify(results[0][0]));
                                resp.end();   
                            }, function( err ) {
                                console.log( "Something bad happened:", err );
                            });
                });  
            } else if (req.url == "/notify/setMax") {
                console.log("set Notification");
                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    schema.setNotifyId ( reqBody.user_id, reqBody.maxNotifyId )
                            .then( function( results ){
                                console.log("Set Notification Id")
                                resp.end();
                            }, function( err ) {
                                console.log( "Something bad happened:", err );
                            });
                });  
            } else if (req.url == "/groups") {
                console.log("Create Group");
                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    var group_name;
                    reqBody.forEach(function(element) {
                        if(element.group_name != undefined){
                            group_name = element.group_name;
                        }
                    }, this);
                    schema.checkGroup( group_name )
                            .then( function( results ){
                                console.log(results[0][0]);
                                if( results[0][0][0] ){
                                    console.log("Group Already Exist !");
                                    resp.write('err');
                                    resp.end();
                                } else {
                                    schema.createGroup( null, reqBody, null, null )
                                        .then( function( results ){
                                            console.log("createGroup Success")
                                            resp.write('200');
                                            resp.end();
                                        }, function( err ) {
                                            console.log( "createGroup Error: ", err );
                                        });
                                }
                            }, function( err ){
                                console.log("checkGroup Error: ", err);
                            } );

                });  
            } else if (req.url == "/groups/get") {
                console.log("Get Groups List");
                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    schema.getGroupsList( reqBody.username )
                            .then( function( results ){
                                console.log(results[0][0]);
                                resp.write(JSON.stringify( results[0][0] ));
                                resp.end();
                            }, function( err ) {
                                console.log( "Something bad happened:", err );
                            });
                });  
            } else if (req.url == "/groups/details") {
                console.log("Get Groups Details");
                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    schema.getGroupsDetails( reqBody.group_name )
                            .then( function( results ){
                                console.log(results[0][0]);
                                resp.write(JSON.stringify( results[0][0] ));
                                resp.end();
                            }, function( err ) {
                                console.log( "Something bad happened:", err );
                            });
                });  
            } else if (req.url == "/groups/add") {
                console.log("Add Group Member");
                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    schema.createGroup( reqBody.uname, reqBody.group_name, reqBody.type, reqBody.mem_type )
                            .then( function( results ){
                                console.log(results);
                                resp.write('200');
                                resp.end();
                            }, function( err ) {
                                console.log( "Something bad happened:", err );
                            });
                });  
            } else if (req.url == "/groups/events") {
                console.log("Fetch Groups Events");
                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    schema.getGroupsEvents( reqBody.group_name )
                            .then( function( results ){
                                console.log( results[0][0] );
                                if( results[0][0][0] != undefined ){
                                    resp.write(JSON.stringify(results[0][0]));
                                } else {
                                    resp.write(JSON.stringify("err"));
                                }
                                resp.end();
                            }, function( err ) {
                                console.log( "Something bad happened:", err );
                            });
                });  
            } else if (req.url == "/review") {
                console.log("Add Event Review");
                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    schema.addEventReview( reqBody.event_name, reqBody.review, reqBody.user_id )
                            .then( function( results ){
                                console.log( results );
                                // if( results[0] != undefined ){
                                //     resp.write(JSON.stringify(results));
                                // } else {
                                //     resp.write(JSON.stringify("err"));
                                // }
                                resp.end();
                            }, function( err ) {
                                console.log( "Something bad happened:", err );
                            });
                });  
            } else if (req.url == "/review/get") {
                console.log("Get Event Review");
                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    schema.getEventReview( reqBody.ev_id )
                            .then( function( results ){
                                console.log( results[0][0] );
                                if( results[0][0][0] != undefined ){
                                    resp.write(JSON.stringify(results[0][0]));
                                } else {
                                    resp.write(JSON.stringify("err"));
                                }
                                resp.end();
                            }, function( err ) {
                                console.log( "Something bad happened:", err );
                            });
                });  
            } else if (req.url == "/request") {
                console.log("Add Request For Event");
                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    schema.addEventRequest( reqBody.req_title, reqBody.req_desc, reqBody.req_cate, reqBody.user_id )
                            .then( function( results ){
                                console.log( results );
                                resp.write("RequestAdded");
                                resp.end();
                            }, function( err ) {
                                console.log( "Something bad happened:", err );
                            });
                });  
            } else if (req.url == "/users/profile") {
                console.log("Get User profile");
                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    schema.getUserProfile( reqBody.uname, reqBody.type )
                            .then( function( results ){
                                console.log( results[0]);
                                resp.write(JSON.stringify(results[0]));
                                resp.end();
                            }, function( err ) {
                                console.log( "Something bad happened:", err );
                            });
                });  
            } else if (req.url == "/event/count") {
                console.log("Get Event count");
                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    schema.getActiveEventCount( reqBody.user_id, reqBody.group )
                            .then( function( results ){
                                console.log( results[0][0][0] );
                                resp.write(JSON.stringify(results[0][0][0]));
                                resp.end();
                            }, function( err ) {
                                console.log( "Something bad happened:", err );
                            });
                });  
            } else if (req.url == "/event/all/count") {
                console.log("Get All Event count");
                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    schema.getAllEventJoined( reqBody.user_id, reqBody.group )
                            .then( function( results ){
                                console.log( results[0][0][0] );
                                resp.write(JSON.stringify(results[0][0][0]));
                                resp.end();
                            }, function( err ) {
                                console.log( "Something bad happened:", err );
                            });
                });  
            } else if (req.url == "/attendance/set") {
                console.log("Set Attendance");
                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    schema.setAttendance( reqBody.ev_id, reqBody.scanData )
                            .then( function( results ){
                                if( results[0][0] == undefined ) {
                                    console.log( results );
                                    resp.write(JSON.stringify('200'));
                                } else if( results[0][0][0].err == 'user not found' ){
                                    console.log( results );
                                    resp.write(JSON.stringify(results[0][0]));
                                }
                                resp.end();
                            }, function( err ) {
                                console.log( "Something bad happened:", err );
                            });
                });  
            } else if (req.url == "/attendance/get") {
                console.log("Get attendance");
                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    schema.getAttendance( reqBody.user_id, reqBody.ev_id )
                            .then( function( results ){
                                console.log( results[0][0] );
                                // resp.write(JSON.stringify(results[0][0][0]));
                                resp.end();
                            }, function( err ) {
                                console.log( "Something bad happened:", err );
                            });
                });  
            } else if (req.url == "/events/get/leader") {
                console.log("Get Event Leader");
                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    schema.getEventLeader( reqBody.ev_id, reqBody.leader_id )
                            .then( function( results ){
                                console.log( results[0][0] );
                                if( reqBody.type == "event" ){
                                    if( results[0][0][0] != undefined ){
                                        resp.write(JSON.stringify(results[0][0][0].ev_leader));
                                    } else {
                                        resp.write(JSON.stringify("err"));
                                    }
                                } else if( reqBody.type == "attendance" ) {
                                    if( results[0][0][0] != undefined ){
                                        resp.write(JSON.stringify(results[0][0]));
                                    } else {
                                        resp.write(JSON.stringify("err"));
                                    }
                                } else {
                                    resp.write(JSON.stringify("EventLeader: NoTypeError"));
                                }
                                resp.end();
                            }, function( err ) {
                                console.log( "Something bad happened:", err );
                            });
                });  
            } else if (req.url == "/events/request") {
                console.log("Get Event Leader");
                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    schema.joinRequest( reqBody.ev_id, reqBody.user_id, reqBody.group_id, reqBody.type )
                            .then( function( results ){
                                console.log( results );
                                resp.write(JSON.stringify('EventRequestSuccess'));
                                resp.end();
                            }, function( err ) {
                                console.log( "Something bad happened:", err );
                            });
                });  
            } else if (req.url == "/events/request/get") {
                console.log("Get Event Leader");
                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    schema.getEvReqStatus( reqBody.user_id, reqBody.group_id )
                            .then( function( results ){
                                console.log( results[0][0] );
                                if( results[0][0][0] != undefined ){
                                    resp.write(JSON.stringify(results[0][0]));
                                } else {
                                    resp.write(JSON.stringify("err"));
                                }
                                resp.end();
                            }, function( err ) {
                                console.log( "Something bad happened:", err );
                            });
                });  
            } else if (req.url == "/events/interest/get") {
                console.log("Get Interest For Event");
                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    schema.getUserInterest( reqBody.user_id )
                            .then( function( results ){
                                console.log( results[0][0] );
                                if( results[0][0][0] != undefined ){
                                    resp.write(JSON.stringify(results[0][0]));
                                } else {
                                    resp.write(JSON.stringify("err"));
                                }
                                resp.end();
                            }, function( err ) {
                                console.log( "Something bad happened:", err );
                            });
                });  
            } else if (req.url == "/events/interest/set") {
                console.log("Set Interest For Evnet");
                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    schema.setInterest( reqBody.user_id, reqBody.interest )
                            .then( function( results ){
                                console.log( results[0][0] );
                                resp.write(JSON.stringify("DONE ADD INTEREST"));
                                resp.end();
                            }, function( err ) {
                                console.log( "Something bad happened:", err );
                            });
                });  
            } else if (req.url == "/events/interest/ev_list") {
                console.log("Get Interest Based Event");
                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse( reqBody );
                    console.log(reqBody);
                    schema.getInterestBasedEvents( reqBody.interest )
                            .then( function( results ){
                                console.log( results[0][0] );
                                if( results[0][0][0] != undefined ){
                                    resp.write(JSON.stringify(results[0][0]));
                                } else {
                                    resp.write(JSON.stringify("err"));
                                }
                                resp.end();
                            }, function( err ) {
                                console.log( "Something bad happened:", err );
                            });
                });  
            } else if (req.url === "/admin/events/add") {
                console.log("Admin insert New Event");

                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse(reqBody);
                    console.log(reqBody.event_date);
                    schema.setAdminEvent( reqBody.event_name, reqBody.event_desc, reqBody.event_city, reqBody.category, reqBody.event_date, reqBody.interest, reqBody.repo_points )  
                        .then( function( results ) {
                            console.log( results);
                            console.log( results[0][0]);
                            resp.write(JSON.stringify("200"));
                            resp.end();
                        }, function( err ) {
                            console.log( "Something bad happened:", err );
                            } );

                });
                
            } else if (req.url === "/admin/users/add") {
                console.log("Admin insert New Member");

                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse(reqBody);
                    schema.setAdminUsers( reqBody.username, reqBody.fullname, reqBody.phone, reqBody.email, reqBody.rfid )  
                        .then( function( results ) {
                            console.log( results);
                            console.log( results[0]);
                            resp.write(JSON.stringify("200"));
                            resp.end();
                        }, function( err ) {
                            console.log( "Something bad happened:", err );
                            } );

                });
                
            } else if (req.url === "/admin/events/users") {
                console.log("get AdminEvents Member");

                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse(reqBody);
                    schema.getAdminEventMembers( reqBody.ev_id )  
                        .then( function( results ) {
                            console.log( results[0][0]);
                            resp.write(JSON.stringify(results[0][0]));
                            resp.end();
                        }, function( err ) {
                            console.log( "Something bad happened:", err );
                            } );

                });
                
            } else if (req.url === "/admin/events/addreqApprove") {
                console.log("get AdminEvents Member");

                var reqBody = "";
                        
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into POST CASE/req.on");
                });
                
                req.on("end", function () {       
                    reqBody = JSON.parse(reqBody);
                    schema.setAdminNewEventReqApprove( reqBody.req_id, reqBody.event_name, reqBody.event_desc, reqBody.category )  
                        .then( function( results ) {
                            console.log( results );
                            resp.write(JSON.stringify('200'));
                            resp.end();
                        }, function( err ) {
                            console.log( "Something bad happened:", err );
                            } );

                });
                
            }        
            break;
        case "PUT":
            console.log("PUT");
            if(req.url === "/users") {
                console.log("update User Info");
                var reqBody = "";
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into PUT CASE/req.on");
                });
                req.on("end", function () {
                    reqBody = JSON.parse(reqBody);
                    console.log(reqBody);
                schema.updateUsers( reqBody.unamex,reqBody.uname,reqBody.fullname,reqBody.passwordx,reqBody.email,reqBody.phone,reqBody.statusx )
                    .then( function( results ) {
                        console.log( results );
                        resp.end();
                    }, function( err ) {
                            console.log( "Something badx happened:", err );
                        } );
                });                
            } else if (req.url === "/events/add") {
                console.log("update Event Details");
                var reqBody = "";
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into PUT CASE/req.on");
                });
                req.on("end", function () {
                    reqBody = JSON.parse(reqBody);
                    console.log(reqBody);
                schema.updateEvents( reqBody.event_namex,reqBody.event_name,reqBody.event_desc,reqBody.event_city )
                    .then( function( results ) {
                        console.log( results );
                        resp.end();
                    }, function( err ) {
                            console.log( "Something badx happened:", err );
                        } );
                });
                
            } else if (req.url === "/events/leader") {
                console.log("update Event leader");
                var reqBody = "";
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into PUT CASE/req.on");
                });
                req.on("end", function () {
                    reqBody = JSON.parse(reqBody);
                    console.log(reqBody);
                schema.setEventLeader( reqBody.lead_id, reqBody.ev_id )
                    .then( function( results ) {
                        console.log( results );
                        resp.write(JSON.stringify("EventLeaderSet"));
                        resp.end();
                    }, function( err ) {
                            console.log( "Something badx happened:", err );
                            resp.write(JSON.stringify("EventLeaderNotSet"));
                            resp.end();
                        } );
                });
            } else if (req.url === "/events/status") {
                console.log("update Event Status");
                var reqBody = "";
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into PUT CASE/req.on");
                });
                req.on("end", function () {
                    reqBody = JSON.parse(reqBody);
                    console.log(reqBody);
                schema.setEventStatus( reqBody.ev_id, reqBody.status )
                    .then( function( results ) {
                        console.log( results );
                        resp.write(JSON.stringify("EventStatusSet"));
                        resp.end();
                    }, function( err ) {
                            console.log( "Something badx happened:", err );
                            resp.write(JSON.stringify("EventStatusNotSet"));
                            resp.end();
                        } );
                });
            } else if (req.url === "/admin/events/update") {
                console.log("AdminUpdate Event Details");
                var reqBody = "";
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into PUT CASE/req.on");
                });
                req.on("end", function () {
                    reqBody = JSON.parse(reqBody);
                    console.log(reqBody);
                schema.updateAdminEvents( reqBody.ev_id,reqBody.event_name,reqBody.event_desc,reqBody.event_city,reqBody.event_date,reqBody.category,reqBody.interest )
                    .then( function( results ) {
                        console.log( results[0][0] );
                        resp.write(JSON.stringify("200"))
                        resp.end();
                    }, function( err ) {
                            console.log( "Something badx happened:", err );
                        } );
                });
                
            } else if (req.url === "/admin/users/update") {
                console.log("AdminUpdate Members Details");
                var reqBody = "";
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into PUT CASE/req.on");
                });
                req.on("end", function () {
                    reqBody = JSON.parse(reqBody);
                    console.log(reqBody);
                schema.updateAdminUsers( reqBody.user_id,reqBody.username,reqBody.fullname,reqBody.phone,reqBody.email,reqBody.rfid )
                    .then( function( results ) {
                        console.log( results[0] );
                        resp.write(JSON.stringify("200"))
                        resp.end();
                    }, function( err ) {
                            console.log( "Something badx happened:", err );
                        } );
                });
                
            } else if (req.url === "/admin/users/allowJoinEvent") {
                console.log("AdminAllow Members For Event");
                var reqBody = "";
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into PUT CASE/req.on");
                });
                req.on("end", function () {
                    reqBody = JSON.parse(reqBody);
                    console.log(reqBody);
                schema.updateAdminAllowEventsJoin( reqBody.user_ev_id, reqBody.user_id)
                    .then( function( results ) {
                        console.log( results[0] );
                        resp.write(JSON.stringify("200"))
                        resp.end();
                    }, function( err ) {
                            console.log( "Something badx happened:", err );
                        } );
                });
                
            } else if (req.url === "/admin/users/rejectJoinEvent") {
                console.log("AdminReject Members For Event");
                var reqBody = "";
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into PUT CASE/req.on");
                });
                req.on("end", function () {
                    reqBody = JSON.parse(reqBody);
                    console.log(reqBody);
                schema.updateAdminRejectEventsJoin( reqBody.user_ev_id, reqBody.user_id)
                    .then( function( results ) {
                        console.log( results[0] );
                        resp.write(JSON.stringify("200"))
                        resp.end();
                    }, function( err ) {
                            console.log( "Something badx happened:", err );
                        } );
                });
                
            }
            // else if (req.url === "/admin/users/lead") {
            //     console.log("AdminLead Members For Event");
            //     var reqBody = "";
            //     req.on("data", function (data) {
            //         reqBody += data;
            //         console.log("into PUT CASE/req.on");
            //     });
            //     req.on("end", function () {
            //         reqBody = JSON.parse(reqBody);
            //         console.log(reqBody);
            //     schema.updateAdminLeadEvent( reqBody.user_ev_id, reqBody.user_id)
            //         .then( function( results ) {
            //             console.log( results[0] );
            //             resp.write(JSON.stringify("200"))
            //             resp.end();
            //         }, function( err ) {
            //                 console.log( "Something badx happened:", err );
            //             } );
            //     });
                
            // }         
            break;
        case "DELETE":
            console.log("DELETE");
            if(req.url === "/users"){
                console.log("Delete User From System");
                var reqBody = "";
                
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into DELETE CASE/req.on");
                });
                
                req.on("end", function () {
                    reqBody = JSON.parse(reqBody);
                    console.log(reqBody);
                    
                    schema.deleteUsersInfo( reqBody.uname )  
                        .then( function( results ) {
                            console.log( results );
                        // var index = 0;
                            // schema.getIFIndex( )
                            //     .then( function( results ) {
                            //         results.forEach(function(element) {
                            //             index = element.id;
                            //             console.log( "forEach: " + index);
                            //         }, this);
                            //     }, function( err ) {
                            //             console.log( "Something bad1 happened:", err );
                            //             resp.end();
                            //         } );
                            
                            // if(index === null ){
                            //         index = 0;
                            //         console.log( "if "+ index);
                            // }
                            
                            // schema.getMax( )
                            //     .then( function( results ){
                            //         console.log(results);
                            //         results.forEach(function(element) {
                            //             index = element.id;
                            //             console.log( "index " + element.id);
                        
                            //             schema.setMax( index )
                            //                 .then( function( results ){
                            //                         console.log( "r2" );
                            //                 }, function( err ){
                            //                         console.log( "Something bad3 happened:", err );
                            //                     } );

                            //         }, this);
                                
                            //     }, function( err ){
                            //             console.log( "Something bad2 happened:", err );
                            //         } );
                                
                            //     if(index === 0){
                            //         schema.setMax( index )
                            //             .then( function( results ){
                            //                     console.log( "r1" );
                            //             }, function( err ){
                            //                     console.log( "Something bad4 happened:", err );
                            //                 } );
                            //     }
                            //     resp.end();
                    }, function( err ) {
                            console.log( "Something bad happened:", err );
                            resp.end();
                        });
                });
            } else if(req.url === "/events/delete") {
                console.log("Delete Event From User Account");
                var reqBody = "";
                
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into DELETE CASE/req.on");
                });
                
                req.on("end", function () {
                    reqBody = JSON.parse(reqBody);
                    console.log(reqBody);
    
                    schema.deleteUsersEvents( reqBody.event_name, reqBody.uname )  
                        .then( function( results ) {
                            console.log( results );
                            resp.write(JSON.stringify(results[0]));
                            resp.end();
                        }, function( err ) {
                                console.log( "Something bad happened:", err );
                                resp.end();
                            } );
                });
            } else if(req.url === "/events/main_delete") {
                console.log("Delete Event From System");
                var reqBody = "";
                
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into DELETE CASE/req.on");
                });
                
                req.on("end", function () {
                    reqBody = JSON.parse(reqBody);
                    console.log(reqBody);
    
                    schema.deleteEvents( reqBody.event_name )  
                        .then( function( results ) {
                            console.log( results );
                            resp.write(JSON.stringify(results[0]));
                            resp.end();
                        }, function( err ) {
                                console.log( "Something bad happened:", err );
                                resp.end();
                            } );
                });
            } else if(req.url === "/groups/delete") {
                console.log("Delete Group");
                var reqBody = "";
                
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into DELETE CASE/req.on");
                });
                
                req.on("end", function () {
                    reqBody = JSON.parse(reqBody);
                    console.log(reqBody);
    
                    schema.deleteGroup( reqBody.group_name )
                        .then( function( results ) {
                            console.log( results[0][0] );
                            resp.write(results[0][0]);
                            resp.end();
                        }, function( err ) {
                                console.log( "Something bad happened:", err );
                                resp.end();
                            } );
                });
            } else if(req.url === "/groups/member/delete") {
                console.log("Delete Group Member");
                var reqBody = "";
                
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into DELETE CASE/req.on");
                });
                
                req.on("end", function () {
                    reqBody = JSON.parse(reqBody);
                    console.log(reqBody);
    
                    schema.deleteGroupMember( reqBody.member, reqBody.group_name )
                        .then( function( results ) {
                            console.log( results[0][0] );
                            resp.write(JSON.stringify("Member Removed"));
                            resp.end();
                        }, function( err ) {
                                console.log( "Something bad happened:", err );
                                resp.end();
                            } );
                });
            } else if(req.url === "/groups/event/delete") {
                console.log("Delete Group Event");
                var reqBody = "";
                
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into DELETE CASE/req.on");
                });
                
                req.on("end", function () {
                    reqBody = JSON.parse(reqBody);
                    console.log(reqBody);
    
                    schema.deleteGroupEvent( reqBody.event_name, reqBody.group_name )
                        .then( function( results ) {
                            console.log( results[0][0] );
                            resp.write("deleted");
                            resp.end();
                        }, function( err ) {
                                console.log( "Something bad happened:", err );
                                resp.end();
                            } );
                });
            } else if(req.url === "/admin/events/delete") {
                console.log("AdminDelete Event");
                var reqBody = "";
                
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into DELETE CASE/req.on");
                });
                
                req.on("end", function () {
                    reqBody = JSON.parse(reqBody);
                    console.log(reqBody);
    
                    schema.deleteAdminEvent( reqBody.ev_id )  
                        .then( function( results ) {
                            console.log( results );
                            resp.write(JSON.stringify(results[0]));
                            resp.end();
                        }, function( err ) {
                                console.log( "Something bad happened:", err );
                                resp.end();
                            } );
                });
            } else if(req.url === "/admin/users/delete") {
                console.log("AdminDelete Members");
                var reqBody = "";
                
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into DELETE CASE/req.on");
                });
                
                req.on("end", function () {
                    reqBody = JSON.parse(reqBody);
                    console.log(reqBody);
    
                    schema.deleteAdminUsers( reqBody.user_id )  
                        .then( function( results ) {
                            console.log( results );
                            resp.write(JSON.stringify(results[0]));
                            resp.end();
                        }, function( err ) {
                                console.log( "Something bad happened:", err );
                                resp.end();
                            } );
                });
            } else if(req.url === "/admin/events/addreqDelete") {
                console.log("AdminDelete Add New Event Request");
                var reqBody = "";
                
                req.on("data", function (data) {
                    reqBody += data;
                    console.log("into DELETE CASE/req.on");
                });
                
                req.on("end", function () {
                    reqBody = JSON.parse(reqBody);
                    console.log(reqBody);
    
                    schema.deleteAdminNewEventReq( reqBody.req_id )  
                        .then( function( results ) {
                            console.log( results );
                            resp.write(JSON.stringify('200'));
                            resp.end();
                        }, function( err ) {
                                console.log( "Something bad happened:", err );
                                resp.end();
                            } );
                });
            }
            break;
        default:
            console.log(req.method, " is NOT Allowed Method !!");
            break;
    }
}) .listen( port, function () {
        console.log( config.server,": \nStarted Listening at: "+port);
    });

//Extra Tests
    // schema.getAllTables()  
    //     .then( function( results ) {
    //         console.log( results );
    //     }, function( err ) {
    //             console.log( "Something bad happened:", err );
    //         } );

    // // Change to match the name of a table
    // var tableName = "users";

    // schema.getTable( tableName )  
    //     .then( function( results ) {
    //         console.log( results );
    //     }, function( err ) {
    //             console.log( "Something bad happened:", err );
    //         } );

    // schema.getColumns( tableName )  
    //     .then( function( results ) {
    //         console.log( results );
    //     }, function( err ) {
    //             console.log( "Something bad happened:", err );
    //         } );