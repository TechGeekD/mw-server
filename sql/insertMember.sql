INSERT INTO [dbo].[users]  
    ( [u_name], [u_full_name], [u_password], [u_email], [u_phone], [u_status] )
VALUES  
    ( @uname, @fullname, @password , @email, @phone, @status );

SELECT u_id AS id  
        , u_name AS uname
        , u_full_name AS fullname
        , u_password AS password
        , u_email AS email
        , u_phone AS phone
        , u_status AS status
FROM     [dbo].[users]  
WHERE     id = SCOPE_IDENTITY();