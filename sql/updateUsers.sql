-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TechGeekd
-- Create date: 14-Jan-17
-- Description:	update users info
-- =============================================
CREATE PROCEDURE updateUsers 
	-- Add the parameters for the stored procedure here
	@unamex nvarchar(50) = 0, 
	@uname nvarchar(50) = 0, 
	@fullname nvarchar(50) = 0,
	@password nvarchar(50) = 0,
	@email nvarchar(50) = 0,
	@phone bigint = 0,
	@status nvarchar(50) = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE users SET u_name = @uname, u_full_name = @fullname, u_password = @password, u_email = @email,
	u_phone = @phone, u_status = @status WHERE u_name = @unamex
END
GO
