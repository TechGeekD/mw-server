var sql = require( "seriate" );
var fs = require('fs');
// var Client = require('ftp');
var JSFtp = require("jsftp");

// var c = new Client();

var user = "boy2sql";

// var config = {
//                 host: "localhost",
//                 user: user,
//                 password: "123"
//             };
// var config = {
//                 host: "www.metta-test2.somee.com",
//                 user: user,
//                 password: "qwertty1!"
//             };

var config = {
  host: "www.metta-test2.somee.com",
  user: user, // defaults to "anonymous" 
  pass: "qwertty1!" // defaults to "@anonymous" 
}

//Admin Handler
    var getAdminAllEventsInfo = function(){
        return sql.execute( {
            procedure: "getAdminAllEventsInfo",
        } );
    }

    var setAdminEvent = function( event_name, event_desc, event_city, category, event_date, interest, repo_points ){
        console.log(event_date);
        return sql.execute( {
            procedure: "setAdminEvent",
            params: {
                name: {
                    type: sql.NVARCHAR,
                    val: event_name
                },
                desc: {
                    type: sql.NVARCHAR,
                    val: event_desc
                },
                city: {
                    type: sql.NVARCHAR,
                    val: event_city
                },
                interest: {
                    type: sql.NVARCHAR,
                    val: interest
                },
                category: {
                    type: sql.NVARCHAR,
                    val: category
                },
                date: {
                    type: sql.DATE,
                    val: event_date
                },
                repo_points: {
                    type: sql.INT,
                    val: repo_points
                }
            }
        } );
    }

    var deleteAdminEvent = function( ev_id ){
        return sql.execute( { 
            procedure: "deleteAdminEvent",
            params: {
                ev_id: {
                    type: sql.INT,
                    val: ev_id
                }
            }
        } );
    }

    var updateAdminEvents = function( ev_id, name, desc, city, date, category, interest ) {

        return sql.execute( {
            procedure: "updateAdminEvents",
            params: {
                ev_id: {
                    type: sql.INT,
                    val: ev_id
                },
                name: {
                    type: sql.NVARCHAR,
                    val: name
                },
                desc: {
                    type: sql.NVARCHAR,
                    val: desc
                },
                city: {
                    type: sql.NVARCHAR,
                    val: city
                },
                date: {
                    type: sql.NVARCHAR,
                    val: date
                },
                category: {
                    type: sql.NVARCHAR,
                    val: category
                },
                interest: {
                    type: sql.NVARCHAR,
                    val: interest
                }                                                
            }
        } );
    }

    var getAdminAllUsersInfo = function(){
        return sql.execute( {
            procedure: "getAdminAllUsersInfo",
        } );
    }

    var setAdminUsers = function( username, fullname, phone, email, rfid ){
        return sql.execute( {
            procedure: "setAdminUsers",
            params: {
                username: {
                    type: sql.NVARCHAR,
                    val: username
                },
                fullname: {
                    type: sql.NVARCHAR,
                    val: fullname
                },
                phone: {
                    type: sql.NVARCHAR,
                    val: phone
                },
                email: {
                    type: sql.NVARCHAR,
                    val: email
                },
                rfid: {
                    type: sql.NVARCHAR,
                    val: rfid
                }
            }
        } );
    }

    var deleteAdminUsers = function( user_id ){
        return sql.execute( { 
            procedure: "deleteAdminUsers",
            params: {
                user_id: {
                    type: sql.INT,
                    val: user_id
                }
            }
        } );
    }

    var updateAdminUsers = function( user_id, username, fullname, phone, email, rfid ) {
        return sql.execute( {
            procedure: "updateAdminUsers",
            params: {
                user_id: {
                    type: sql.INT,
                    val: user_id
                },
                username: {
                    type: sql.NVARCHAR,
                    val: username
                },
                fullname: {
                    type: sql.NVARCHAR,
                    val: fullname
                },
                phone: {
                    type: sql.NVARCHAR,
                    val: phone
                },
                email: {
                    type: sql.NVARCHAR,
                    val: email
                },
                rfid: {
                    type: sql.NVARCHAR,
                    val: rfid
                }                                                
            }
        } );
    }

    var getAdminEventMembers = function( ev_id ){
        return sql.execute( { 
            procedure: "getAdminEventMembers",
            params: {
                ev_id: {
                    type: sql.INT,
                    val: ev_id
                }
            }
        } );
    }

    var getAdminAllEventsRequest = function( ){
        return sql.execute( {
            procedure: "getAdminAllEventsRequest",
        } );
    }

    var getAdminAllEventJoinReq = function( ){
        return sql.execute( {
            procedure: "getAdminAllEventJoinReq",
        } );
    }
    
    var updateAdminAllowEventsJoin = function( user_ev_id, user_id ){
        return sql.execute( { 
            procedure: "updateAdminAllowEventsJoin",
            params: {
                user_ev_id: {
                    type: sql.INT,
                    val: user_ev_id
                },
                user_id: {
                    type: sql.INT,
                    val: user_id
                }
            }
        } );
    }

    var updateAdminRejectEventsJoin = function( user_ev_id, user_id ){
        return sql.execute( { 
            procedure: "updateAdminRejectEventsJoin",
            params: {
                user_ev_id: {
                    type: sql.INT,
                    val: user_ev_id
                },
                user_id: {
                    type: sql.INT,
                    val: user_id
                }
            }
        } );
    }

    var getAdminStats = function( ){
        return sql.execute( {
            procedure: "getAdminStats",
        } );
    }

    var setAdminNewEventReqApprove = function ( req_id, event_name, event_desc, category ){
        return sql.execute( {
            procedure: 'setAdminNewEventReqApprove',
            params: {
                event_name: {
                    type: sql.NVARCHAR,
                    val: event_name
                },
                event_desc: {
                    type: sql.NVARCHAR,
                    val: event_desc
                },
                category: {
                    type: sql.NVARCHAR,
                    val: category
                },
                req_id: {
                    type: sql.INT,
                    val: req_id
                }
            }
        });
    }

    var deleteAdminNewEventReq = function ( req_id ){
        return sql.execute( {
            procedure: 'deleteAdminNewEventReq',
            params: {
                req_id: {
                    type: sql.INT,
                    val: req_id
                }
            }
        });
    }

//User Handler
    var updateUsers = function( unamex, uname, fullname, password, email, phone, status ) {  
        return sql.execute( {
            procedure: "updateUsers",
            params: {
                unamex: {
                    type: sql.NVARCHAR,
                    val: unamex
                },
                uname: {
                    type: sql.NVARCHAR,
                    val: uname
                },
                fullname: {
                    type: sql.NVARCHAR,
                    val: fullname
                },
                password: {
                    type: sql.NVARCHAR,
                    val: password
                },
                email: {
                    type: sql.NVARCHAR,
                    val: email
                },
                phone: {
                    type: sql.BIGINT,
                    val: phone
                },
                status: {
                    type: sql.NVARCHAR,
                    val: status
                }
            }
        } );
    };

    var getUserProfile = function( uname, type ) {
        return sql.execute( {
            procedure: "getUsersInfo",
            params: {
                uname: {
                    type: sql.NVARCHAR,
                    val: uname
                },
                type: {
                    type: sql.NVARCHAR,
                    val: type
                }
            }
        } );
    };

    var insertUsersInfo = function( uname, fullname, password, email, phone, status ) {  
        return sql.execute( {
            procedure: "insertUsersInfo",
            params: {
                uname: {
                    type: sql.NVARCHAR,
                    val: uname
                },
                fullname: {
                    type: sql.NVARCHAR,
                    val: fullname
                },
                password: {
                    type: sql.NVARCHAR,
                    val: password
                },
                email: {
                    type: sql.NVARCHAR,
                    val: email
                },
                phone: {
                    type: sql.BIGINT,
                    val: phone
                },
                status: {
                    type: sql.NVARCHAR,
                    val: status
                }
            }
        } );
    };

    var deleteUsersInfo = function( uname ) {  
        return sql.execute( {
            procedure: "deleteUsersInfo",
            params: {
                uname: {
                    type: sql.NVARCHAR,
                    val: uname
                }
            }
        } );
    };

    var getAllUsersInfo = function( ) {  
        return sql.execute( {
            procedure: "getAllUsersInfo",
        } );
    };

    var findUser = function ( uname ) {
        return sql.execute ( {
            procedure: "findUser",
            params: {
                uname: {
                    type: sql.NVARCHAR,
                    val: uname
                }
            }
        } );
    }

    // var getIFIndex = function( ) {  
        //     return sql.execute( {
        //         query: "SELECT id FROM users WHERE id = 1"
        //     } );
        // };

        // var getMax = function( ){
        //     return sql.execute( {
        //         query: "SELECT id FROM users WHERE id = (SELECT MAX(id) FROM users)"
        //     });
        // }

        // var setMax = function( index ){
        //     console.log("set: "+index);
        //     return sql.execute( {
        //         query: "DBCC CHECKIDENT ('users', RESEED, @index)",
        //         params: {
        //             index: {
        //                 type: sql.INT,
        //                 val: index
        //             }
        //         }
        //     } );
    // }

    var loginUsers = function( uname, password ){

        return sql.execute( {
            procedure: "loginUsers",
            params: {
                uname: {
                    type: sql.NVARCHAR,
                    val: uname
                },
                password: {
                    type: sql.NVARCHAR,
                    val: password
                }
            }
        } );

    }

    var setAttendance = function( ev_id, scanData ){
        return sql.execute ( {
            procedure: "setAttendance",
            params: {
                ev_id: {
                    type: sql.NVARCHAR,
                    val: ev_id
                },
                scanData: {
                    type: sql.NVARCHAR,
                    val: scanData
                }
            }
        } );
    }

    var getAttendance = function( user_id, ev_id ) {
        return sql.execute( {
            procedure: "getAttendance",
            params: {
                user_id: {
                    type: sql.INT,
                    val: user_id
                },
                ev_id: {
                    type: sql.INT,
                    val: ev_id
                }
            }
        } );
    }

    var setFpAuthToken = function( user_id, token ){
        return sql.execute ( {
            procedure: "setFpAuthToken",
            params: {
                user_id: {
                    type: sql.NVARCHAR,
                    val: user_id
                },
                token: {
                    type: sql.NVARCHAR,
                    val: token
                }
            }
        } );
    }

    var getAllFpAuthToken = function( ){
        return sql.execute({
            procedure: "getAllFpAuthToken"
        } );
    }

//Events Handler
    var getAllEventsInfo = function( category ) {  
        return sql.execute( {
            procedure: "getAllEventsInfo",
            params: {
                category: {
                    type: sql.NVARCHAR,
                    val: category
                }
            }
        } );
    };

    var getEventsDetails = function ( ev_id ) {
        return sql.execute ( {
            procedure: "getEventsDetails",
            params: {
                ev_id: {
                    type: sql.INT,
                    val: ev_id
                }
            }
        } );
    }

    var getEventsParticipant = function ( ev_id ) {
        return sql.execute({
            procedure: "getEventsParticipant",
            params: {
                ev_id: {
                    type: sql.INT,
                    val: ev_id
                }
            }
        } );
    }

    var insertEventInfo = function( eventname, eventdesc, eventcity, category ) {
        return sql.execute( {
            procedure: "insertEventInfo",
            params: {
                event_name: {
                    type: sql.NVARCHAR,
                    val: eventname
                },
                event_desc: {
                    type: sql.NVARCHAR,
                    val: eventdesc
                },
                event_city: {
                    type: sql.NVARCHAR,
                    val: eventcity
                },
                type: {
                    type: sql.NVARCHAR,
                    val: "event"
                },
                category: {
                    type: sql.NVARCHAR,
                    val: category
                }
            }
        } );
    }

    var updateEvents = function( event_namex, event_name, event_desc, event_city ) {  
        return sql.execute( {
            procedure: "updateEvents",
            params: {
                event_namex: {
                    type: sql.NVARCHAR,
                    val: event_namex
                },
                event_name: {
                    type: sql.NVARCHAR,
                    val: event_name
                },
                event_desc: {
                    type: sql.NVARCHAR,
                    val: event_desc
                },
                event_city: {
                    type: sql.NVARCHAR,
                    val: event_city
                }
            }
        } );
    };

    var joinEvents = function( user_id, group_id, ev_id, type ) {
        return sql.execute( {
            procedure: "joinEvents",
            params: {
                user_id: {
                    type: sql.INT,
                    val: user_id
                },
                group_id: {
                    type: sql.INT,
                    val: group_id
                },
                ev_id: {
                    type: sql.INT,
                    val: ev_id
                },
                type: {
                    type: sql.NVARCHAR,
                    val: type
                }
            }
        } );
    };

    var getAllUsersEvents = function( uname ) {  
        return sql.execute( {
            procedure: "getAllUsersEvents",
            params: {
                uname: {
                    type: sql.NVARCHAR,
                    val: uname
                }
            }
        } );
    };

    var deleteUsersEvents = function( event_name, uname ){
        return sql.execute( { 
            procedure: "deleteUsersEvents",
            params: {
                uname: {
                    type: sql.NVARCHAR,
                    val: uname
                },
                event_name: {
                    type: sql.NVARCHAR,
                    val: event_name
                }
            }
        } );

    };

    var deleteEvents = function( event_name ){
        return sql.execute( { 
            procedure: "deleteEvents",
            params: {
                event_name: {
                    type: sql.NVARCHAR,
                    val: event_name
                }
            }
        } );

    };

    var addEventRequest = function( req_title, req_desc, req_cate ,user_id ){
        return sql.execute({
            procedure: "addEventRequest",
            params: {
                req_title: {
                    type: sql.NVARCHAR,
                    val: req_title
                },
                req_desc: {
                    type: sql.NVARCHAR,
                    val: req_desc
                },
                req_cate: {
                    type: sql.NVARCHAR,
                    val: req_cate
                },
                user_id: {
                    type: sql.INT,
                    val: user_id
                }
            }
        } );
    }

    var getEventRequest = function( ){
        return sql.execute({
            procedure: "getEventRequest"
        } );
    }

    var getActiveEventCount = function( user_id, group ) {
        return sql.execute({
            procedure: "getActiveEventCount",
            params: {
                user_id: {
                    type: sql.NVARCHAR,
                    val: user_id
                },
                group: {
                    type: sql.NVARCHAR,
                    val: group
                }
            }
        } );
    }

    var getAllEventJoined = function( user_id, group ) {
        return sql.execute({
            procedure: "getAllEventJoinedCount",
            params: {
                user_id: {
                    type: sql.NVARCHAR,
                    val: user_id
                },
                group: {
                    type: sql.NVARCHAR,
                    val: group
                }
            }
        } );
    }

    var setEventLeader = function( ev_leader, ev_id ) {
        return sql.execute({
            procedure: "setEventLeader",
            params: {
                ev_leader: {
                    type: sql.INT,
                    val: ev_leader
                },
                ev_id: {
                    type: sql.INT,
                    val: ev_id
                }
            }
        } );
    }

    var setEventStatus = function( ev_id, status ) {
        return sql.execute({
            procedure: "setEventStatus",
            params: {
                ev_id: {
                    type: sql.INT,
                    val: ev_id
                },
                status: {
                    type: sql.NVARCHAR,
                    val: status
                }
            }
        } );
    }

    var getEventLeader = function( ev_id, leader_id ) {
        return sql.execute({
            procedure: "getEventLeader",
            params: {
                ev_id: {
                    type: sql.INT,
                    val: ev_id
                },
                leader_id: {
                    type: sql.INT,
                    val: leader_id
                }
            }
        } );
    }

    var joinRequest = function( ev_id, user_id, group_id, type ) {
        return sql.execute({
            procedure: "joinRequest",
            params: {
                ev_id: {
                    type: sql.INT,
                    val: ev_id
                },
                user_id: {
                    type: sql.INT,
                    val: user_id
                },
                group_id: {
                    type: sql.INT,
                    val: group_id
                },
                type: {
                    type: sql.NVARCHAR,
                    val: type
                }
            }
        } )
    }

    var getEvReqStatus = function( user_id, group_id ){
        return sql.execute({
            procedure: "getEvReqStatus",
            params: {
                user_id: {
                    type: sql.INT,
                    val: user_id
                },
                group_id: {
                    type: sql.INT,
                    val: group_id
                }
            }
        } );
    }

    var getAllInterest = function(){
        return sql.execute({
            procedure: "getAllInterest"
        } );
    }

    var getUserInterest = function( user_id ){
        return sql.execute({
            procedure: "getUserInterest",
            params: {
                user_id: {
                    type: sql.INT, 
                    val: user_id,
                }
            }
        } );
    }

    var setInterest = function( user_id, interest ){
        return sql.execute({
            procedure: "setUserInterest",
            params: {
                user_id: {
                    type: sql.INT, 
                    val: user_id,
                },
                interest: {
                    type: sql.NVARCHAR,
                    val: interest
                }
            }
        } );
    }

    var getInterestBasedEvents = function( interest ){
        return sql.execute({
            procedure: "getInterestBasedEvents",
            params: {
                interest: {
                    type: sql.NVARCHAR,
                    val: interest
                } 
            }
        } );
    }

//Media Handler
    var manageImage = function ( imgData, username, event_name ) {
        
        var Ftp = new JSFtp(config);
        var img = imgData.replace(/^data:image\/\w+;base64,/, "");
            console.log("image Data Fetched");
            var buff = new Buffer(img, 'base64');
            var dt = new Date();
            var dbExec = undefined;

            if(event_name == "NULL"){
                var dir = 'www.metta-test2.somee.com/media/image/users/'+username+'/';
            } else {
                var dir = 'www.metta-test2.somee.com/media/image/events/'+event_name+'/';
            }
            
            var saveImg = dir+dt.getTime()+".jpeg";
        console.log(saveImg);

        //OLD BUGGY FTP SETUP
        // c.status(function(err, status){
            //     status = status.substr(status.search('user: ')+6);
            //     var end = status.indexOf('\r');
            //     var ftp_user = status.substr(0,end);
            //     console.log(ftp_user)
            //     if( ftp_user != user  ){
            //         c.connect(config);
            //     }
            // });
            // c.connect(config);
            // c.on('ready', function() {
            // console.log("ready");
                // c.list( dir, function(err, list) {
                //     console.log("data");
                //     if (err) {
                //         c.mkdir( dir, true, function(err) { 
                //             if(err) { 
                //                 throw err; 
                //             } else {
                //                 c.get(saveImg, function(err, stream) {
                //                     if (err){
                //                         c.put( buff, saveImg, function(err) {
                //                             if (err) throw err;
                //                             console.log("image Saved", saveImg);                                
                //                         });
                //                     } else {
                //                         console.log(saveImg, "exist");
                //                     }
                //                 });
                //             }
                //         });
                //     } else {
                //         c.get(saveImg, function(err, stream) {
                //             if (err){
                //                 c.put( buff, saveImg, function(err) {
                //                     if (err) throw err;
                //                     console.log("image Saved", saveImg);                                
                //                 });
                //             } else {
                //                 console.log(saveImg, "exist");
                //             }
                //         });
                //     }
                // });  
                // c.end();    
        // });

        //NEW FTP SETUP
        Ftp.on('error', function(err){
            console.log(err);
        });

        Ftp.ls(dir, function(err, res) {
            if(!err){
                Ftp.raw("cwd", dir, function(err, data) {
                    if (err) return console.error(err);
                    console.log(data.text); // Show the FTP response text to the user
                    console.log(data.code); // Show the FTP response code to the user
                    res.forEach(function(file) {
                        Ftp.raw("dele", file.name, function(err, data) {
                            if (err) return console.error(err);
                            console.log("Deleted: ",file.name);
                            console.log(data.text); // Show the FTP response text to the user
                            console.log(data.code); // Show the FTP response code to the user
                        });
                    });
                    Ftp.raw("cwd", "/", function(err, data) {
                        if (err) return console.error(err);
                        console.log(data.text); // Show the FTP response text to the user
                        console.log(data.code); // Show the FTP response code to the user
                        Ftp.raw("pwd", function(err, data) {
                            if (err) return console.error(err);
                            console.log(data.text); // Show the FTP response text to the user
                            console.log(data.code); // Show the FTP response code to the user
                            Ftp.put(buff, saveImg, function(hadError) {
                                if (!hadError) {
                                    console.log("Image Updloaded:",saveImg);
                                    dbExec = true;
                                } else {
                                    dbExec = false;
                                }
                            });
                        });
                    });
                });
            } else {
                Ftp.raw("mkd", dir, function(err, data) {
                    if (err) return console.error(err);

                    console.log(data.text); // Show the FTP response text to the user
                    console.log(data.code); // Show the FTP response code to the user
                    Ftp.put(buff, saveImg, function(hadError) {
                        if (!hadError) {
                            console.log("Image Updloaded:",saveImg);
                            dbExec = true;
                        } else {
                            dbExec = false;
                        }
                    });
                });
            }
        });


        var time = setInterval(()=>{
            console.log("Wait");
            if( dbExec != undefined ){                
                clearInterval(time)
                console.log("Done!!!!");
                if( dbExec == true ){
                    Ftp.raw("quit", function(err, data) {
                        if (err) return console.error(err);
                        console.log("Bye!");
                    });
                }
                if( event_name == "NULL"){
                    return sql.execute( {
                        procedure: "manageImageUser",
                        params: {
                            saveImg: {
                                type: sql.NVARCHAR,
                                val: "http://"+saveImg
                            },
                            username: {
                                type: sql.NVARCHAR,
                                val: username
                            }
                        }
                    } );
                } else if( dbExec == true ) {
                    return sql.execute( {
                        procedure: "manageImageEvent",
                        params: {
                            saveImg: {
                                type: sql.NVARCHAR,
                                val: "http://"+saveImg
                            },
                            event_name: {
                                type: sql.NVARCHAR,
                                val: event_name
                            }
                        }
                    } );
                } else if( dbExec == false ) {
                    return dbExec;
                }
            }
        }, 3000);
    }

    var insertMedia = function( media, med_type, category, ev_id, event_name ) {

        var Ftp = new JSFtp(config);
        var media = media.replace(/^data:image\/\w+;base64,/, "");
        console.log("Fetched Image Data");

        var buff = new Buffer(media, 'base64');
        var dt = new Date();
        var dbExec = undefined;

        var dir = 'www.metta-test2.somee.com/media/image/events/'+event_name+'/';

        var saveImg = dir+dt.getTime()+".jpeg";

        //OLD BUGGY FTP SETUP
        // c.connect(config);
        // c.on('ready', function() {
            // c.list( dir, function(err, list) {
            //     if (err) {
            //         c.mkdir( dir, true, function(err) { 
            //             if(err) { 
            //                 throw err; 
            //             } else {
            //                 c.put( buff, saveImg, function(err) {
            //                     if (err) throw err;
            //                     console.log("image Saved", saveImg);
            //                 } );
            //             }
            //         });
            //     } else {
            //         c.put( buff, saveImg, function(err) {
            //             if (err) throw err;
            //             console.log("image Saved", saveImg);
            //         } );
            //     }
            //     c.end();
            // });
        // });

        //NEW FTP SETUP
        Ftp.on('error', function(err){
            console.log(err);
        });

        Ftp.ls(dir, function(err, res) {
            if(!err){
                Ftp.put(buff, saveImg, function(hadError) {
                    if (!hadError) {
                        console.log("Image Updloaded:",saveImg);
                        dbExec = true;
                    } else {
                        dbExec = false;
                    }
                });
            } else {
                Ftp.raw("mkd", dir, function(err, data) {
                    if (err) {
                        dbExec = false;
                        return console.error(err);
                    } else {
                        console.log(data.text); // Show the FTP response text to the user
                        console.log(data.code); // Show the FTP response code to the user
                        Ftp.put(buff, saveImg, function(hadError) {
                            if (!hadError) {
                                console.log("Image Updloaded:",saveImg);
                                dbExec = true;
                            } else {
                                dbExec = false;
                            }
                        });
                    }
                });
            }
        });

        var time = setInterval(() =>{
            console.log("Wait");
            if( dbExec != undefined ){
                console.log("Done!!");
                clearInterval(time);
                if( dbExec == true ){
                    return sql.execute({
                        procedure: "insertMedia",
                        params: {
                            media: {
                                type: sql.NVARCHAR,
                                val: 'http://'+saveImg
                            },
                            med_type: {
                                type: sql.NVARCHAR,
                                val: med_type
                            },
                            category: {
                                type: sql.NVARCHAR,
                                val: category
                            },
                            ev_id: {
                                type: sql.INT,
                                val: ev_id
                            },
                        }
                    } );
                } else {
                    return dbExec;
                }
            }
        },3000);
    }

    var getMedia = function( ev_id, type ) {
        return sql.execute({
            procedure: "getMedia",
            params: {
                ev_id: {
                    type: sql.INT,
                    val: ev_id
                },
                type: {
                    type: sql.NVARCHAR,
                    val: type
                }
            }
        } );
    }

    var getPic = function ( username, event_name ) {

        if( event_name == "NULL" ) {
            console.log("user pic");
            return sql.execute ( {
                procedure: "getPicUser",
                params: {
                    username: {
                        type: sql.NVARCHAR,
                        val: username
                    }
                }
            } );

        } else {
            console.log("event pic");
            return sql.execute ( {
                procedure: "getPicEvent",
                params: {
                    event_name: {
                        type: sql.NVARCHAR,
                        val: event_name
                    }
                }
            } );

        }
    }

//System Event Handler
    // var getMaxNotifyId = function ( ) {
            // return sql.execute({
            //     query: "SELECT nid FROM notification WHERE nid = (SELECT MAX(nid) FROM notification)"
            // } );
    // }

    var setNotifyId = function ( user_id, maxNid ) {
            return sql.execute({
                procedure: "setNotifyId",
                params: {
                    user_id: {
                        type: sql.INT,
                        val: user_id
                    },
                    maxNid: {
                        type: sql.INT,
                        val: maxNid
                    }
                }
            });
    }

    var getNotify = function ( user_id, maxNid ){
        return sql.execute( {
            procedure: "getNotify",
            params: {
                user_id: {
                    type: sql.INT,
                    val: user_id
                },
                maxNid: {
                    type: sql.INT,
                    val: maxNid
                }
            }
        } );   
    }

    var addEventReview = function( ev_id, review, user_id ){
        return sql.execute({
            procedure: "addEventReview",
            params: {
                ev_id: {
                    type: sql.INT,
                    val: ev_id
                },
                review: {
                    type: sql.NVARCHAR,
                    val: review
                },
                user_id: {
                    type: sql.INT,
                    val: user_id
                }
            }
        } );
    }

    var getEventReview = function ( ev_id ){
        return sql.execute({
            procedure: "getEventReview",
            params: {
                ev_id: {
                    type: sql.INT,
                    val: ev_id
                }
            }
        } );
    }

//Groups Handler
    var createGroup = function ( username, group, type, mem_type ){
        if( type != "addMember" ){
            var group_name;
            var sql_str = "INSERT INTO groups ( member, group_name, mem_type ) VALUES";
            group.forEach(function(element) {
                if(element.group_name != undefined){
                    group_name = element.group_name;
                }
            }, this);

            group.forEach(function(element) {
                sql_str += " ( '" + element.uname + "', '" + group_name + "', '"+ element.mem_type +"' ),";
            }, this);
            
            var n = sql_str.search("undefined");
            sql_str = sql_str.slice(0, n-5);
        } else if( type == 'addMember' ){
            var sql_str = "INSERT INTO groups ( member, group_name, mem_type ) VALUES ( '" + username + "', '" + group + "', '" + mem_type + "')";
        } else {
            console.log("createGroup Error Type Not Found !");
        }
        console.log(sql_str);
        return sql.execute( {
            query: sql_str
        } );
    }

    var checkGroup = function ( group_name ){
        return sql.execute({
            procedure: "checkGroup",
            params: {
                group_name: {
                    type: sql.NVARCHAR,
                    val: group_name
                }
            }
        } );
    }

    var getGroupsList = function ( username ){
        return sql.execute( {
            procedure: "getGroupsList",
            params: {
                username: {
                    type: sql.NVARCHAR,
                    val: username
                }
            }
        } );
    }

    var getGroupsDetails = function ( group_name ){
        return sql.execute({
            procedure: "getGroupsDetails",
            params: {
                group_name: {
                    type: sql.NVARCHAR,
                    val: group_name
                }
            }
        } );
    }

    var getGroupsEvents = function( group_name ){
        return sql.execute({
            procedure: "getGroupsEvents",
            params: {
                group_name: {
                    type: sql.NVARCHAR,
                    val: group_name
                }
            }
        } );
    }

    var deleteGroup = function ( group_name ) {
        return sql.execute({
            procedure: "deleteGroup",
            params: {
                group_name: {
                    type: sql.NVARCHAR,
                    val: group_name
                }
            }
        } );
    }

    var deleteGroupMember = function( member, group_name ){
        return sql.execute({
            procedure: "deleteGroupMember",
            params: {
                member: {
                    type: sql.NVARCHAR,
                    val: member
                }, 
                group_name: {
                    type: sql.NVARCHAR,
                    val: group_name
                }
            }
        } );
    }

    var deleteGroupEvent = function( event_name, group_name ){
        return sql.execute({
            procedure: "deleteGroupEvent",
            params: {
                event_name: {
                    type: sql.NVARCHAR,
                    val: event_name
                },
                group_name: {
                    type: sql.NVARCHAR,
                    val: group_name
                }
            }
        } );
    }

//Extra Tests
    // var sendJSON = function (req, resp, data) {
    //     resp.writeHead(200, { "content-type": "application/json" });
    //     if (data) {
    //         resp.write(JSON.stringify(data));
    //     }
        
    //     resp.end();
    // };

    // var getAllTables = function() {  
    //     return sql.execute( {
    //         query: sql.fromFile( "./sql/getAllTables" )
    //     } );
    // };

    // var getTable = function( tableName ) {  
    //     return sql.execute( {
    //         query: sql.fromFile( "./sql/getTableByName" ),
    //         params: {
    //             tableName: {
    //                 type: sql.NVARCHAR,
    //                 val: tableName
    //             }
    //         }
    //     } );
    // };

    // var getColumns = function( tableName ) {  
    //     return sql.execute( {
    //         query: sql.fromFile( "./sql/getColumnsByTableName" ),
    //         params: {
    //             tableName: {
    //                 type: sql.NVARCHAR,
    //                 val: tableName
    //             }
    //         }
    //     } );
    // };

module.exports = {  
    updateUsers: updateUsers,
    getUserProfile: getUserProfile,
    insertUsersInfo: insertUsersInfo,
    deleteUsersInfo: deleteUsersInfo,
    getAllUsersInfo: getAllUsersInfo,
    // getIFIndex: getIFIndex,
    // getMax: getMax,
    // setMax: setMax,
    loginUsers: loginUsers,
    getAllEventsInfo: getAllEventsInfo,
    insertEventInfo: insertEventInfo,
    updateEvents: updateEvents,
    joinEvents: joinEvents,
    getAllUsersEvents: getAllUsersEvents,
    deleteUsersEvents: deleteUsersEvents,
    deleteEvents: deleteEvents,
    getEventsDetails: getEventsDetails,
    getEventsParticipant: getEventsParticipant,
    manageImage: manageImage,
    getPic: getPic,
    getNotify: getNotify,
    findUser: findUser,
    setNotifyId: setNotifyId,
    checkGroup: checkGroup,
    createGroup: createGroup,
    getGroupsList: getGroupsList,
    getGroupsDetails: getGroupsDetails,
    deleteGroup: deleteGroup,
    deleteGroupMember: deleteGroupMember,
    getGroupsEvents: getGroupsEvents,
    deleteGroupEvent: deleteGroupEvent,
    addEventReview: addEventReview,
    getEventReview: getEventReview,
    addEventRequest: addEventRequest,
    getEventRequest: getEventRequest,
    insertMedia: insertMedia,
    getMedia: getMedia,
    getActiveEventCount: getActiveEventCount,
    getAllEventJoined: getAllEventJoined,
    setAttendance: setAttendance,
    getAttendance: getAttendance,
    setEventLeader: setEventLeader,
    setEventStatus: setEventStatus,
    getEventLeader: getEventLeader,
    joinRequest: joinRequest,
    getEvReqStatus: getEvReqStatus,
    getAllInterest: getAllInterest,
    getUserInterest: getUserInterest,
    setInterest: setInterest,
    getInterestBasedEvents: getInterestBasedEvents,
    setFpAuthToken: setFpAuthToken,
    getAllFpAuthToken: getAllFpAuthToken,
    getAdminAllEventsInfo: getAdminAllEventsInfo,
    setAdminEvent: setAdminEvent,
    deleteAdminEvent: deleteAdminEvent,
    updateAdminEvents: updateAdminEvents,
    getAdminAllUsersInfo: getAdminAllUsersInfo,
    setAdminUsers: setAdminUsers,
    deleteAdminUsers: deleteAdminUsers,
    updateAdminUsers: updateAdminUsers,
    getAdminEventMembers: getAdminEventMembers,
    getAdminAllEventsRequest: getAdminAllEventsRequest,
    getAdminAllEventJoinReq: getAdminAllEventJoinReq,
    updateAdminAllowEventsJoin: updateAdminAllowEventsJoin,
    updateAdminRejectEventsJoin: updateAdminRejectEventsJoin,
    setAdminNewEventReqApprove: setAdminNewEventReqApprove,
    deleteAdminNewEventReq: deleteAdminNewEventReq,
    getAdminStats: getAdminStats,
    // getMaxNotifyId: getMaxNotifyId
    //sendJSON: sendJSON
    // getAllTables: getAllTables,
    // getTable: getTable,
    // getColumns: getColumns
};