var sql = require( "seriate" );  
var when = require( "when" );

var insertUsersTransaction = function( uname, fullname, password, email, phone, status ) {  
    return when.promise( function( resolve, reject ) {
        // getTransactionContext() returns a SQL connection with
        // a transaction
        sql.getTransactionContext()
            // The first step takes an alias and query object
            .step( "insertMember", {
                query: sql.fromFile( "./sql/insertMember" ),
                params: {
                    uname: { type: sql.NVARCHAR, val: uname },
                    fullname: { type: sql.NVARCHAR, val: fullname },
                    password: { type: sql.NVARCHAR, val: password },
                    email: { type: sql.NVARCHAR, val: email },
                    phone: { type: sql.NVARCHAR, val: phone },
                    status: { type: sql.NVARCHAR, val: status }
                }
            } )
            // Each step afterwards expects an alias and a function
            // The current execute context is passed along with the
            // results from the previous step. The results are available
            // using the alias name.
            /*.step( "addTwitterProfile", function( execute, data ) {
                var memberId = data.insertMember[ 0 ].id;
                execute( {
                    query: sql.fromFile( "./sql/insertMemberProfile" ),
                    params: {
                        memberId: { type: sql.INT, val: memberId },
                        key: { type: sql.NVARCHAR, val: "Twitter" },
                        value: { type: sql.NVARCHAR, val: twitter }
                    }
                } );
            } )*/
            .end( function( result ) {
                // In the end, result has commit() and rollback()
                // functions, and a property named "sets" that contains
                // the results of all previous steps.
                result.transaction
                    .commit()
                    .then( function() {
                        var member = result.sets.insertMember[ 0 ];
                        //member.twitter = result.sets.addTwitterProfile[ 0 ].value;
                        resolve( member );
                    }, function( err ) {
                            reject( err );
                        } );
            } )
            .error( function( err ) {
                reject( err );
            } );
    } );
};

var getMember = function( uid ) {  
    return when.promise( function( resolve, reject ) {
        // getPlainContext() returns a SQL connection without
        // a transaction
        sql.getPlainContext()
            .step( "getMemberById", {
                query: sql.fromFile( "./sql/getMemberById" ),
                params: {
                    uid: { type: sql.INT, val: uid }
                }
            } )
            /*.step( "getProfile", function( execute, data ) {
                execute( {
                    query: sql.fromFile( "./sql/getMemberProfileById" ),
                    params: {
                        memberId: { type: sql.INT, val: memberId }
                    }
                } );
            } )*/
            .end( function( sets ) {
                // In the end, a plain context will return 'sets' with
                // a property named after each of the aliased steps
                var member = sets.getMemberById[ 0 ];
                //member.profile = {};
                //var profile = sets.getProfile;
                // for (var i = 0; i < profile.length; i++) {
                //     member.profile[ profile[ i ].key ] = profile[ i ].value;
                // }
                resolve( member );
            } )
            .error( function( err ) {
                reject( err );
            } );
    } );
};

module.exports = {  
    getMember: getMember,
    insertMemberTransaction: insertMemberTransaction
};